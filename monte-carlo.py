import numpy as np

def f(x):
    return 2 * (x**3) + 5 * (x**2) - (3 * x) - 1 

sizes = [100, 500, 1000, 2000, 3000]

x1 = 1
x2 = 3

for size in sizes:
    print(sum(map(lambda x: f(x), np.random.uniform(x1, x2, size=size))) * (x2 - x1) / size)
