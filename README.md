# Monte Carlo


## Zadanie


Proszę policzyć za pomocą metody Monte-Carlo całkę oznaczoną z wymyślonej przez siebie funkcji. Przeprowadzić symulacje dla n=100, 500, 1000, 2000, 3000. Porównać wyniki obliczeń z dokładną wartością całki.

```math
f(x) = 2x^3+5x^2-3x-1
```
```math
∫_1^3 f(x)ⅆx = 69 \frac{1}{3}
```
## Odpowiedzi

Uzyskane rozwiązania:

|n	|Wynik symulacji	|Błąd|
|-------|-------------------|-----------------|
|100	|71.71832412593852	|2.384990792605194|
|500	|67.65616636158802	|1.677166971745308|
|1000	|68.35615941278921	|0.9771739205441179|
|2000	|68.78539616866541	|0.5479371646679141|
|3000	|68.78302048078312	|0.5503128525502063|

Szacowane wartości są bliskie wartości z całki. Największa dokładność występuje dla dużych wartości n, n=2000 i n=3000.
